<?php namespace PHPRouter;

class Routes{
	
	/**
	 *
	 * @var RouteCollection
	 */
	private $routes;
	/**
	 *
	 * @var Router
	 */
	private $router;
	/**
	 *
	 * @var boolean | Route
	 */
	private $currentRoute;
	/**
	 * The route configuration
	 * @var object
	 */
	private $config;
	/**
	 * The path to the route configuration file
	 * @var string
	 */
	private $configPath;
	
	/**
	 * Routes constructor
	 * @param string $configpath
	 */
	public function __construct($configpath){
		$this->routes = new RouteCollection();
		$this->configPath = $configpath;
	}
	
	/**
	 * Configure the defined routes
	 */
	public function config(){
		
		$config = Config::loadFromFile($this->configPath);
		
		$this->config = (object) $config;
		
		$this->router = Router::parseConfig($config);
		
		$this->currentRoute = $this->router->matchCurrentRequest();
		
		$this->routes = $this->router->getRouteCollection();
	}
	
	/**
	 * Retrieve the current active route
	 * @return boolean | Route
	 */
	public function getCurrentRoute(){
		return $this->currentRoute;
	}
	
	/**
	 * Retrieve the set of defined module API routes
	 * @return \PHPRouter\RouteCollection
	 */
	public function getRoutes(){
		return $this->routes;
	}
	
	/**
	 * Retrieve the routes config object
	 * @return object|StdClass
	 */
	public function getConfig(){
		return $this->config;
	}
}

?>